import React from 'react'

const WelcomePage = () => {
    //WELCOME PAGE
    return (
        <div>
            <h1>Welcome To Digital Sticky Notes!</h1>
        </div>
    )
}

export default WelcomePage